$(document).ready(function() {

	$('#input-location-search').keyup(function() {
		
		query = $(this).val();
		
		loadPart({
			url: "queryLocation",
			data: "query=" + query,
			domElement: "#location-search-results"
		})
		
	})
	
})

function saveCity(cityCode) {
	
	postAjax({
		url: "saveLocation",
		data: "cityCode=" + cityCode,
		success: function() {
			loadPart({
				url: "mycities",
				data: "listOnly=true",
				domElement: "#mycities-list"
			})
		}
	})
	
}

function removeCity(cityCode) {
	
	postAjax({
		url: "removeLocation",
		data: "cityCode=" + cityCode,
		success: function() {
			loadPart({
				url: "mycities",
				data: "listOnly=true",
				domElement: "#mycities-list"
			})
		}
	})
	
}


/*
 * Custom Ajax Post function, that catches backend notifications
 * 
 * Options:
 * +url: post request url
 * data: data to be sent
 * success: success function
 * error: success error
 * 
 */
function loadPart(args) {
	var type = "GET";
	var url = (args.url && args.url.length > 0) ? args.url : "";
	var data = (args.data) ? args.data : {};
	var success = args.success;
	var error = args.error;
	var append = (args.append) ? args.append : false;
	var $domElement = $(args.domElement);
	if (typeof(success) != "function") {
		success = function(){};
	}
	if (typeof(error) != "function") {
		error = function(){};
	}
	
	$.ajax({
		type: type,
		url: url,
		data: data,
		headers: {"partialLoad":true},
		success: function(data){
			if (append) $domElement.append(data);
			else $domElement.html(data);
			success();
		},
		error: function(){
			error();
		}, 
		complete: function(xhr, status) {
			//console.log(status)
			//console.log(xhr)
		}
		
	})
}

/*
 * Custom Ajax Post function, that catches backend notifications
 * 
 * Options:
 * +url: post request url
 * data: data to be sent
 * success: success function
 * error: success error
 * 
 */
function postAjax(args) {
	var type = "POST";
	var url = (args.url && args.url.length > 0) ? args.url : "";
	var data = (args.data) ? args.data : {};
	var success = args.success;
	var error = args.error;
	if (typeof(success) != "function") {
		success = function(){};
	}
	if (typeof(error) != "function") {
		error = function(){};
	}
	$.ajax({
		type: type,
		url: url,
		data: data,
		success: function(){
			success();
		},
		error: function(){
			error();
		}
		
	})
}