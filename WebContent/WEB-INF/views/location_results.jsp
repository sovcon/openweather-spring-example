<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<table class="table table-striped">

	<c:forEach items="${queryResponse.getResults()}" var="result">
		<tr> 
			<td>
				<c:if test="${!empty result.getName() }"><span class="city">${result.getName()}</span><br/></c:if>
				<c:if test="${!empty result.getPlz() }"><span class="index">${result.getPlz() }</span><br/></c:if>
				<c:if test="${!empty result.getQuarter()}"><span class="quarter">${result.getQuarter()}</span><br/></c:if>
				<c:if test="${!empty result.getRegionName() }"><span class="region">${result.getRegionName()}</span><br/></c:if>
				<c:if test="${!empty result.getStateName() }"><span class="state">${result.getStateName()}</span><br/></c:if>
				<c:if test="${!empty result.getCountryCode() }"><span class="country">${result.getCountryCode()}</span></c:if>
			</td>
			<td><button type="button" class="btn btn-default pull-right" onclick="saveCity('${result.getCityCode()}')"><i class="fa fa-plus"></i></button></td>
		
		</tr>
	</c:forEach>

</table>