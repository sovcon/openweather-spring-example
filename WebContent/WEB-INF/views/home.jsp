<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1 class="page-header">${pageInfo.pageTitle()}</h1>

	<c:forEach items="${forecastMap}" var="entry">
		<c:set var="location" value="${entry.key}" />
		<c:set var="forecast" value="${entry.value}" />
		<div class="row">
			<div class="col-md-12">
				<h2>${location.getName()}</h2>
				<div class="row">
					<c:forEach items="${forecast.getDays()}" var="day">
						<div class="col-md-4">
							<div class="row">
								<div class="col-xs-12 day-average">
									<h3>${dateTimeUtils.date(day.getDate()).formatDate("E, dd MMMM") }</h3>
									<span class="weather-status-text">${day.getWeatherStatusText() }</span>
									<i data-weather-code="${day.getWeatherStatus()}"
										class="${WeatherIcons.getAPIWeatherStatus(day.getWeatherStatus()) } weather-status large"></i>
									<br /> <span class="temperature"> ${day.getMinTemp() } /
										<b>${day.getMaxTemp() } <i class="wi wi-celsius"></i>
									</b>
									</span> <br /> 
									<span class="wind">
										Wind: ${day.getWindDirectionText() } 
										<i class="${WeatherIcons.getWindDirectionIcon(day.getWindDirection()) }"></i>
									</span>
									<span class="wind-speed">
										${day.getWindSpeed() } km/h <br />
									</span>
								</div>
							</div>
							<div class="row">
								<c:forEach items="${day.getForecastTimes() }" var="map"
									varStatus="i">
									<c:set var="time" value="${map.key}" />
									<c:set var="forecast" value="${map.value }" />
									<div class="col-md-3 time-${i.index}">
										<h4>
											<i class="${WeatherIcons.getTimeIcon(forecast.getTimestampUTC()) }"></i>
											${time }
										</h4>
										<span class="weather-status-text">${forecast.getWeatherStatusText() }</span>
										<i data-weather-code="${forecast.getWeatherStatus()}"
											class="${WeatherIcons.getAPIWeatherStatus(forecast.getWeatherStatus()) } weather-status"></i><br />
										<span class="temperature">${forecast.getMinTemp() } / <b>${forecast.getMaxTemp() } <i
											class="wi wi-celsius"></i></b></span> <br />
										<span class="wind">
											Wind: <br/> ${forecast.getWindDirectionText() } 
											<i class="${WeatherIcons.getWindDirectionIcon(forecast.getWindDirection()) }"></i>
										</span>
										<span class="wind-speed">
											${forecast.getWindSpeed() } km/h <br />
										</span>
									</div>
								</c:forEach>
							</div>
						</div>
					</c:forEach>
				</div>
	
			</div>
	
		</div>
	</c:forEach>
	
	<c:if test="${forecastMap.size() == 0 }">
		<div class="well text-danger">No forecasts are available at the moment because you haven't added any locations yet. Go <a href="mycities">here</a> to add a location.</div>
	</c:if>


</body>
</html>
