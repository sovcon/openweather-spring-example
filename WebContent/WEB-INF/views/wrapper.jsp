<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="<c:url value="/assets/css/bootstrap.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/assets/css/bootstrap-theme.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/assets/css/font-awesome.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/assets/css/weather-icons.min.css"/>">

<link rel="stylesheet"
	href="<c:url value="/assets/css/weatherdemo.css"/>">

<script type="text/javascript"
	src="<c:url value="/assets/js/jquery-2.1.3.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/assets/js/bootstrap.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/assets/js/weatherdemo.js"/>"></script>
</head>

<body>

	<div class="container-fluid">

		<div class="row">

			<div class="col-sm-2 col-md-1 sidebar">
				<ul class="nav nav-sidebar">

					<c:forEach items="${pages.getPageInfos()}" var="page">

						<c:set var="url" value="${page.key}" />
						<c:set var="info" value="${page.value}" />

						<li
							class="${info.menuPosition() eq pageInfo.menuPosition() ? 'active' : ''}">
							<a href="<c:url value="${url}" />">${info.pageTitle()}</a>
						</li>

					</c:forEach>
				</ul>
			</div>



			<div class="col-sm-10 col-sm-offset-2 col-md-11 col-md-offset-1 main">
				<div id="app-info">
					<span class="badge pull-right">openweather API - Java SDK AppDemo</span>
				</div>
				<jsp:include page="${pageInfo.pageFileName()}" />

			</div>

		</div>


	</div>
	
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-61785513-1', 'auto');
		ga('send', 'pageview');
	
	</script>

</body>
</html>
