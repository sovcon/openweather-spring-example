<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${savedCities.size() != 0}">
	<table class="table table-striped">
	
	
			<c:forEach items="${savedCities}" var="city">
				<tr>
					<td>
						
						<c:if test="${!empty city.getName() }"><span class="city">${city.getName()}</span><br/></c:if>
						<c:if test="${!empty city.getPlz() }"><span class="index">${city.getPlz() }</span><br/></c:if>
						<c:if test="${!empty city.getQuarter()}"><span class="quarter">${city.getQuarter()}</span><br/></c:if>
						<c:if test="${!empty city.getRegionName() }"><span class="region">${city.getRegionName()}</span><br/></c:if>
						<c:if test="${!empty city.getStateName() }"><span class="state">${city.getStateName()}</span><br/></c:if>
						<c:if test="${!empty city.getCountryCode() }"><span class="country">${city.getCountryCode()}</span></c:if>
						
					</td>
					<td class="align-right">
						<button type="button" class="btn btn-default pull-right"
							onclick="removeCity('${city.getCityCode()}')">
							<i class="fa fa-remove"></i>
						</button>
					</td>
		
				</tr>
			</c:forEach>
		
	
	</table>
</c:if>
<c:if test="${savedCities.size() == 0}">
	<div class="well text-danger">No locations available yet. Search for a location on the
			right to get a weather forecast.		
	</div>
</c:if>