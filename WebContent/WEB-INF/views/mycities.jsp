<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1 class="page-header">${pageInfo.pageTitle()} <small>manage weather locations</small></h1>

<div class="row">
	<div class="col-md-6 col-sm-12" id="mycities-list">
		<jsp:include page="list.mycities.jsp" />
	</div>

	<div class="col-md-6 col-sm-12">
		<form class="form-horizontal">
			<div class="form-group">
				<label for="input-location-search" class="col-sm-4 control-label">Search
					a Location</label>
				<div class="col-sm-8">
					<input type="email" class="form-control" id="input-location-search"
						placeholder="Search Term">
				</div>
			</div>
		</form>

		<div id="location-search-results">
			<jsp:include page="location_results.jsp" />
		</div>
	</div>
</div>