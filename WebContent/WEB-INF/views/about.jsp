<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1 class="page-header">${pageInfo.pageTitle()}</h1>

<h2>About this demo</h2>
<p>This is a demo application making use of the <a href="https://bitbucket.org/sovcon/openweather-java-sdk/overview">Java SDK</a> that I've written for the wetter.com openweather API.</p>
<p>As this web application is only meant for demonstration purposes to show what the SDK + API can do, the front-end / design of this demo was not given a lot of attention. </p>
<p>You can check out the source to <a href="https://bitbucket.org/sovcon/openweather-spring-example/">this demo at bitbucket</a>.</p>

<h2>About the wetter.com API</h2>
<p>I had to make use of wetter.com data in a private Java project, so I decided I could as well write a little SDK as exercise. Now that it's there, I'm sharing it, just in case.</p>
<p>I'm not even sure there is demand for a SDK to wetter.com's API since there are better API alternatives available. - The wetter.com API is not documented so well,
does not offer that much data and doesn't have full international coverage. 
As far as I understood from their (German only) <a href="http://www.wetter.com/apps_und_mehr/website/api/dokumentation/">documentation</a>, 
you can set the output language to English, German and Spanish. 
Obviously this demo uses the English output, but I still noticed some German Strings (e.g. getWindDirectionText() returns "O" for east) so keep that in mind.</p>
<p>I still think it can be an OK choice for a quick weather lookup. If it says anything, my girlfriend finds their forecast info precise :)</p>

<h2>Technology used</h2>
<p>This demo is a Spring MVC Application running on a tomcat server. The location and weather data is queried via my Java SDK from the wetter.com API. 
User selected locations are stored in the session. <br/>The Frontend is a basic Bootstrap 3 setup and stuff is loaded in over ajax.</p>

<h2>Credit</h2>
<p><a href="http://erikflowers.github.io/weather-icons/" target="_blank">Weather Icons</a></p>
<p><a href="http://www.wetter.com" target="_blank">Powered by wetter.com</a></p>