package com.sladit.openweatherExample.util;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.apache.log4j.Logger;

public final class WeatherIcons {
	
	private static final Logger log = Logger.getLogger(WeatherIcons.class);
	
	public static final String WIND_DEFAULT = "wi wi-wind-default";
	
	public static final class WindDirections {
		public static final String
			WIND_DEFAULT_0_DEG = WIND_DEFAULT + " _0-deg",
			WIND_DEFAULT_15_DEG = WIND_DEFAULT + " _15-deg",
			WIND_DEFAULT_30_DEG = WIND_DEFAULT + " _30-deg",
			WIND_DEFAULT_45_DEG = WIND_DEFAULT + " _45-deg",
			WIND_DEFAULT_60_DEG = WIND_DEFAULT + " _60-deg",
			WIND_DEFAULT_75_DEG = WIND_DEFAULT + " _75-deg",
			WIND_DEFAULT_90_DEG = WIND_DEFAULT + " _90-deg",
			WIND_DEFAULT_105_DEG = WIND_DEFAULT + " _105-deg",
			WIND_DEFAULT_120_DEG = WIND_DEFAULT + " _120-deg",
			WIND_DEFAULT_135_DEG = WIND_DEFAULT + " _135-deg",
			WIND_DEFAULT_150_DEG = WIND_DEFAULT + " _150-deg",
			WIND_DEFAULT_165_DEG = WIND_DEFAULT + " _165-deg",
			WIND_DEFAULT_180_DEG = WIND_DEFAULT + " _180-deg",
			WIND_DEFAULT_195_DEG = WIND_DEFAULT + " _195-deg",
			WIND_DEFAULT_210_DEG = WIND_DEFAULT + " _210-deg",
			WIND_DEFAULT_225_DEG = WIND_DEFAULT + " _225-deg",
			WIND_DEFAULT_240_DEG = WIND_DEFAULT + " _240-deg",
			WIND_DEFAULT_255_DEG = WIND_DEFAULT + " _255-deg",
			WIND_DEFAULT_270_DEG = WIND_DEFAULT + " _270-deg",
			WIND_DEFAULT_285_DEG = WIND_DEFAULT + " _285-deg",
			WIND_DEFAULT_300_DEG = WIND_DEFAULT + " _300-deg",
			WIND_DEFAULT_315_DEG = WIND_DEFAULT + " _315-deg",
			WIND_DEFAULT_330_DEG = WIND_DEFAULT + " _330-deg",
			WIND_DEFAULT_345_DEG = WIND_DEFAULT + " _345-deg";
	}
	
	public static final class Clocks {
		public static final String
			TIME_0 = "wi wi-time-12",
			TIME_1 = "wi wi-time-1",
			TIME_2 = "wi wi-time-2",
			TIME_3 = "wi wi-time-3",
			TIME_4 = "wi wi-time-4",
			TIME_5 = "wi wi-time-5",
			TIME_6 = "wi wi-time-6",
			TIME_7 = "wi wi-time-7",
			TIME_8 = "wi wi-time-8",
			TIME_9 = "wi wi-time-9",
			TIME_10 = "wi wi-time-10",
			TIME_11 = "wi wi-time-11",
			TIME_12 = TIME_0,
			TIME_13 = TIME_1,
			TIME_14 = TIME_2,
			TIME_15 = TIME_3,
			TIME_16 = TIME_4,
			TIME_17 = TIME_5,
			TIME_18 = TIME_6,
			TIME_19 = TIME_7,
			TIME_20 = TIME_8, 
			TIME_21 = TIME_9,
			TIME_22 = TIME_10,
			TIME_23 = TIME_11,
			TIME_24 = TIME_0;	
	}
	
	public static final class WeatherStatus {
		public static final String
			SUNNY = "wi wi-day-sunny",
			CLOUDY = "wi wi-cloudy",
			CLOUD = "wi wi-cloud",
			FOG = "wi wi-fog",
			SHOWERS = "wi wi-showers",
			SPRINKLE = "wi wi-sprinkle",
			RAIN = "wi wi-rain",
			SNOW = "wi wi-snow",
			THUNDERSTORM = "wi wi-thunderstorm";
	}
	
	public static String getAPIWeatherStatus(String weatherCode) {
		int mainCode = Character.getNumericValue(weatherCode.charAt(0));
		switch (mainCode) {
			case 0: return WeatherStatus.SUNNY; // sunny / clear
			case 1: return WeatherStatus.CLOUD; // partly cloudy
			case 2: return WeatherStatus.CLOUDY; // cloudy
			case 3: return WeatherStatus.CLOUDY; // covered in clouds
			case 4: return WeatherStatus.FOG; // fog
			case 5: return WeatherStatus.SPRINKLE; // sprinkle 
			case 6: return WeatherStatus.RAIN; // rain
			case 7: return WeatherStatus.SHOWERS; // showers
			case 8: return WeatherStatus.SNOW; // snow
			case 9: return WeatherStatus.THUNDERSTORM; // thunder
			default: return null;
		}
	}
	
	public static String getWindDirectionIcon(int deg) {
		
		for (Field f : WindDirections.class.getFields()) {
			if (f.getName().contains(String.valueOf(deg))) {
				try {
					return (String) f.get(String.class);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					return null;
				}
			}
		}
		return null;
	}
	
	public static String getTimeIcon(String hours) {
		for (Field f : Clocks.class.getFields()) {
			if (f.getName().endsWith(hours)) {
				try {
					return (String) f.get(String.class);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					return null;
				}
			}
		}
		return null;
	}
	
	public static String getTimeIcon(Timestamp timestamp) {
		SimpleDateFormat format = new SimpleDateFormat("H", Locale.US);
		String timeString = format.format(timestamp);
		return getTimeIcon(timeString);
	}
}
