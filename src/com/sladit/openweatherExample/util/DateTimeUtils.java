package com.sladit.openweatherExample.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Component;

@Component
public class DateTimeUtils {
	
	private Date date;
	
	public DateTimeUtils date(Date date) {
		this.date = date;
		return this;
	}
	
	private Timestamp timestamp;
	
	public DateTimeUtils timestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
		return this;
	}
	
	/**
	 * Requires {{@link #date} to be set.
	 * @param pattern
	 * @return date formatted date String
	 */
	public String formatDate(String pattern) {
		
		SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.US);
		String dateString = format.format(date);
		
		return dateString;
	}
	
	/**
	 * Requires {{@link #timestamp} to be set.
	 * @param pattern
	 * @return date formatted date String
	 */
	public String formatTimestamp(String pattern) {
		
		SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.US);
		String dateString = format.format(timestamp);
		
		return dateString;
	}
	
}
