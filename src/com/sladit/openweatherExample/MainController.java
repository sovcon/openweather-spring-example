package com.sladit.openweatherExample;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sladit.openweather.*;
import com.sladit.openweather.json.forecast.Forecast;
import com.sladit.openweather.json.location.LocationResponse;
import com.sladit.openweather.json.location.LocationResult;
import com.sladit.openweatherExample.annotations.PageInfo;

@Controller
public class MainController {
	
	public static final Authenticator AUTHENTICATOR = new Authenticator("projectName", "apiKey");

	private @Autowired UserWeatherPreference preference;
	private @Autowired PageInfoStorage pageInfos;
	
	private static final Logger log = Logger.getLogger(MainController.class);
	
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	@PageInfo(pageFileName="home.jsp", pageTitle="Forecast", menuPosition=0)
	public String home(Model model, @RequestParam(required=false) boolean ajaxLoad) {
		PageInfo pageInfo = pageInfos.get("/");
		model.addAttribute("pageInfo", pageInfo);
	
		OpenweatherConnection connection = new OpenweatherConnection(AUTHENTICATOR); 
		
		Map<LocationResult, Forecast> forecastMap = new HashMap<>(); 
		
		preference.getSavedCities().forEach(l -> {
			try {
				forecastMap.put(l, connection.queryForecast(l.getCityCode()).getCity().getForecast());
			} catch (Exception e) {
				log.error("Error querying forecast for [" + l.getCityCode() + "]: " + e);
			}
		});
		
		model.addAttribute("forecastMap", forecastMap);
		
		
		if (ajaxLoad) {
			return pageInfo.pageFileName();
		}
		
		return "wrapper";
	}
	
	@RequestMapping(value="/mycities", method = RequestMethod.GET)
	@PageInfo(pageFileName="mycities.jsp", pageTitle="My Cities", menuPosition=1)
	public String myCities(Model model, 
			@RequestParam(required=false) boolean ajaxLoad,
			@RequestParam(required=false) boolean listOnly) {
		PageInfo pageInfo = pageInfos.get("/mycities");
		model.addAttribute("pageInfo", pageInfo);
		
		model.addAttribute("savedCities", preference.getSavedCities());
		
		if (listOnly) {
			return "list.mycities";
		}
		if (ajaxLoad) {
			return pageInfo.pageFileName();
		}
		
		return "wrapper";
	}
	
	@RequestMapping(value="/about", method = RequestMethod.GET)
	@PageInfo(pageFileName="about.jsp", pageTitle="About", menuPosition=2)
	public String about(Model model, @RequestParam(required=false) boolean ajaxLoad) {
		PageInfo pageInfo = pageInfos.get("/about");
		model.addAttribute("pageInfo", pageInfo);
		
		
		
		if (ajaxLoad) {
			return pageInfo.pageFileName();
		}
		
		return "wrapper";
	}
	
	
	
	@RequestMapping(value="/saveLocation", method = RequestMethod.POST)
	public @ResponseBody String saveLocationPreference(Model model, @RequestParam(required = true) String cityCode) {
		preference.addCity(cityCode);
		return "";
	}
	
	@RequestMapping(value="/removeLocation", method = RequestMethod.POST)
	public @ResponseBody String removeLocationPreference(Model model, @RequestParam(required = true) String cityCode) {
		preference.removeCity(cityCode);
		return "";
	}
	
	@RequestMapping(value = "/queryLocation", method = RequestMethod.GET)
	public String queryLocation(Model model, @RequestParam(required = true) String query) {
			
		OpenweatherConnection connection = new OpenweatherConnection(AUTHENTICATOR); 
		
		LocationResponse queryResponse = null;
		
		try {
		
			queryResponse = connection.queryLocation(query);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// iterate queryResponse.getResults();
		
		model.addAttribute("queryResponse", queryResponse);
		
		return "location_results";
	}
	
	
	
}
