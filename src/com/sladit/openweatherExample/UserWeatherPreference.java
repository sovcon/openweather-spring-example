package com.sladit.openweatherExample;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.sladit.openweather.Authenticator;
import com.sladit.openweather.OpenweatherConnection;
import com.sladit.openweather.json.location.LocationResponse;
import com.sladit.openweather.json.location.LocationResult;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserWeatherPreference {
	
	private Set<LocationResult> savedCities = new HashSet<LocationResult>();

	public Set<LocationResult> getSavedCities() {
		return savedCities;
	}

	public void setSavedCities(Set<LocationResult> savedCities) {
		this.savedCities = savedCities;
	}
	
	public void addCity(LocationResult city) {
		this.savedCities.add(city);
	}
	
	public void addCity(String cityCode) {
		OpenweatherConnection connection = new OpenweatherConnection(MainController.AUTHENTICATOR); 
		
		LocationResponse queryResponse = null;
		
		try {
		
			queryResponse = connection.queryLocation(cityCode);
			savedCities.add(queryResponse.getResults().get(0));
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void removeCity(String cityCode) {
		savedCities.removeIf(l -> l.getCityCode().equals(cityCode));
	}
	
}
