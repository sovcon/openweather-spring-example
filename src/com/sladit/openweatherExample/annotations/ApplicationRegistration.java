package com.sladit.openweatherExample.annotations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.sladit.openweatherExample.PageInfoStorage;

@Component
public class ApplicationRegistration implements ApplicationListener<ContextRefreshedEvent> {

	private @Autowired ApplicationContext context;
	private @Autowired PageInfoStorage pageInfoStorage;

	private Logger log = LoggerFactory.getLogger(ApplicationRegistration.class);

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		//log.debug(event.getApplicationContext().getId());
		
		if (event.getApplicationContext().equals(context)) {
		
			pageInfoStorage.setPageInfos(RequestMappingScanner.scan(context));

			log.info("[Registration] done.");
			
		}
	}

}
