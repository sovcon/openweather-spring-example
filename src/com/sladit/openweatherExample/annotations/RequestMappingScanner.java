package com.sladit.openweatherExample.annotations;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class RequestMappingScanner {

	private static final Logger log = LoggerFactory
			.getLogger(RequestMappingScanner.class);

	/**
	 * Returns a map of all urls, their shortcut, and their needed activity for
	 * a given controller class and module name.
	 * 
	 * @param clazz
	 * @param module
	 * @return
	 */
	public static Map<String, PageInfo> scan(ApplicationContext context) {

		Map<String, Object> beans = context
				.getBeansWithAnnotation(Controller.class);

		List<Class<?>> controllers = new ArrayList<>();
		for (Entry<String, Object> bean : beans.entrySet()) {
			// Okay this is a kinda dirty hack because in the context are partly
			// cglib enhanced controllers
			String className = bean.getValue().getClass().getName();
			String[] split = className.split("\\$");
			try {
				controllers.add(Class.forName(split[0], true,
						context.getClassLoader()));
			} catch (ClassNotFoundException e) {
				log.error("Error getting class when scanning for controllers.",
						e);
			}
		}

		TreeMap<String, PageInfo> pageInfos = new TreeMap<>();
		for (Class<?> clazz : controllers) {

			Method[] methods = clazz.getDeclaredMethods();

			for (Method method : methods) {

				RequestMapping requestMapping = method
						.getAnnotation(RequestMapping.class);
				if (requestMapping != null
						&& containsGET(requestMapping.method())) {

					//log.debug("requestMapping: " + requestMapping.value()[0]);

					PageInfo pageInfo = method.getAnnotation(PageInfo.class);

					//log.debug("pageInfo:" + pageInfo);

					if (pageInfo != null) {
						//log.debug(pageInfo.pageFileName());
						pageInfos.put(requestMapping.value()[0], pageInfo);
					}

				}
			}
		}

		Map<String, PageInfo> sortedByMenuValue = pageInfos
				.entrySet()
				.stream()
				.sorted(menuPositionComparator)
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue,
                             (e1, e2) -> e1, LinkedHashMap::new));


		return sortedByMenuValue;
	}

	private static boolean containsGET(RequestMethod[] methods) {
		if (methods.length == 0) {
			return true; // Nothing maps to everything
		}
		for (RequestMethod method : methods) {
			if (method.equals(RequestMethod.GET)) {
				return true;
			}
		}
		return false;
	}

	private static Comparator<Entry<String, PageInfo>> menuPositionComparator = (e1, e2) -> e1.getValue().menuPosition() - e2.getValue().menuPosition();

}
