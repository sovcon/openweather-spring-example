package com.sladit.openweatherExample.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value=ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PageInfo {
	String pageFileName();
    String pageTitle();
    int menuPosition() default 0;
}
