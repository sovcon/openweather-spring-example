package com.sladit.openweatherExample;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sladit.openweatherExample.util.DateTimeUtils;
import com.sladit.openweatherExample.util.WeatherIcons;

public class CommonModelAttributesInterceptor  extends HandlerInterceptorAdapter {

	private @Autowired PageInfoStorage pageInfos;
	private @Autowired DateTimeUtils dateTimeUtils;
	
	public void postHandle(
			HttpServletRequest request, HttpServletResponse response, 
			Object handler, ModelAndView modelAndView)
			throws Exception {
		
		if (modelAndView != null) {
            modelAndView.getModelMap().addAttribute("pages", pageInfos);
            modelAndView.getModelMap().addAttribute("dateTimeUtils", dateTimeUtils);
            modelAndView.getModelMap().addAttribute("WeatherIcons", new WeatherIcons());
        } 
		
	}
}