package com.sladit.openweatherExample;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sladit.openweatherExample.annotations.PageInfo;

@Component
public class PageInfoStorage {

	private static final Logger log = Logger.getLogger(PageInfoStorage.class);
	
	private Map<String, PageInfo> pageInfos;

	public Map<String, PageInfo> getPageInfos() {
		return pageInfos;
	}

	public void setPageInfos(Map<String, PageInfo> pageInfos) {
		this.pageInfos = pageInfos;
	}
	
	public PageInfo get(String requestMapping) {
		return pageInfos.get(requestMapping);
	}

}
