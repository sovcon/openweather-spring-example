# openweather-java-sdk demo #
In this repo you can find the source to the demo spring web application for my [openweather-java-sdk](https://bitbucket.org/sovcon/openweather-java-sdk/overview).

The demo can be found [here](http://openweather.sladit.com)